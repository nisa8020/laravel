<!doctype html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
    <head>
        <meta charset="UTF-8">
        <title>Welcome to the task</title>
        <style>
            @import url(//fonts.googleapis.com/css?family=Lato:700);

            body {
                margin:0;
                font-family:'Lato', sans-serif;
                text-align:center;
                color: #999;
            }

            .header {
                width: 100%;
                left: 0px;
                top: 5%;
                text-align: left;
                border-bottom: 1px  #999 solid;
            }

            .student-table{
                width:100%;  
            }

            table.student-table th{
                background-color: #C6C6C6;
                text-align: left;
                color: white;
                padding:7px 3px;
                font-weight: 700;
                font-size: 18px;
            }

            table.student-table tr.odd {
                text-align: left;
                padding:5px;
                background-color: #F9F9F9;
            }

            table.student-table td{
                text-align: left;
                padding:5px;
            }

            a, a:visited {
                text-decoration:none;
                color: #999;
            }

            h1 {
                font-size: 32px;
                margin: 16px 0 0 0;
            }
        </style>
        <script type="text/javascript">
            checked=false;
            function checkedAll (form_course) {
                var aa= document.getElementById('form_course');
                if (checked == false){
                    checked = true
                }else{
                    checked = false
                }
                for (var i =0; i < aa.elements.length; i++){
                    aa.elements[i].checked = checked;
                }
            }
        </script>
    </head>

    <body>

    <form id="form_course" method="POST" action="/export/course">
        {{ csrf_field() }}
        <div class="header">
            <div><img src="/images/logo_sm.jpg" alt="Logo" title="logo"></div>
            <div  style='margin: 10px;  text-align: left'>
                <button  onclick="this.form.submit()">Export Curse</button>
            </div>
            <div  style='margin: 10px;  text-align: left'>
                <a href="{{url('view')}}"> Export Student</a>
            </div>
        </div>



            <div style='margin: 10px; text-align: center;'>
                <table class="student-table">
                    <tr>
                        <th><input type='checkbox' name='checkall' onclick='checkedAll(form_course);'></th>
                        <th>Course Name</th>
                        <th>University</th>
                        <th>Students count</th>
                    </tr>

                    @if(  count($course) > 0 )
                    @foreach($course as $value)
                    <tr>
                        <td><input type="checkbox" name="courseId[]" value="{{$value['id']}}"></td>
                        <td style=' text-align: left;'>{{$value['course_name']}}</td>
                        <td style=' text-align: left;'>{{$value['university']}}</td>
                        <td style=' text-align: left;'>{{count ($value['students'])}}</td>
                        {{--<td style=' text-align: left;'>{{$student['surname']}}</td>--}}
                        {{--<td style=' text-align: left;'>{{$student['email']}}</td>--}}
                        {{--<td style=' text-align: left;'>{{$student['course']['university']}}</td>--}}
                        {{--<td style=' text-align: left;'>{{$student['course']['course_name']}}</td>--}}
                        {{--<td style=' text-align: left;'>{{$student['address']['city']}}</td>--}}
                        {{--<td style=' text-align: left;'>{{$student['address']['postcode']}}</td>--}}
                        {{--<td style=' text-align: left;'>{{$student['address']['line_1'].' / '.$student['address']['line_2']}}</td>--}}
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="9" style="text-align: center">Oh dear, no data found.</td>
                    </tr>
                    @endif
                </table>
            </div>

        </form>


    </body>

</html>

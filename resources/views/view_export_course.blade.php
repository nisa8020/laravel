<!doctype html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
    <head>
        <meta charset="UTF-8">
        <title>Welcome to the task</title>
        <style>
            @import url(//fonts.googleapis.com/css?family=Lato:700);

            body {
                margin:0;
                font-family:'Lato', sans-serif;
                text-align:center;
                color: #999;
            }

            .header {
                width: 100%;
                left: 0px;
                top: 5%;
                text-align: left;
                border-bottom: 1px  #999 solid;
            }

            .student-table{
                width:100%;  
            }

            table.student-table th{
                background-color: #C6C6C6;
                text-align: left;
                color: white;
                padding:7px 3px;
                font-weight: 700;
                font-size: 18px;
            }

            table.student-table tr.odd {
                text-align: left;
                padding:5px;
                background-color: #F9F9F9;
            }

            table.student-table td{
                text-align: left;
                padding:5px;
            }

            a, a:visited {
                text-decoration:none;
                color: #999;
            }

            h1 {
                font-size: 32px;
                margin: 16px 0 0 0;
            }
        </style>
        <script type="text/javascript">
            checked=false;
            function checkedAll (form_student) {
                var aa= document.getElementById('form_student');
                if (checked == false){
                    checked = true
                }else{
                    checked = false
                }
                for (var i =0; i < aa.elements.length; i++){
                    aa.elements[i].checked = checked;
                }
            }
        </script>
    </head>

    <body>

    <form id="form_student" method="POST" action="/export">
        {{ csrf_field() }}
        <div class="header">
            <div><img src="/images/logo_sm.jpg" alt="Logo" title="logo"></div>
            <div  style='margin: 10px;  text-align: left'>
                @if(  !empty($url) )
                    <div  style='margin: 10px;  text-align: left'>
                          <a href="{{'/'.$url}}">Download</a>
                    </div>
                @endif
                    <div  style='margin: 10px;  text-align: left'>
                         <a href="{{url('course')}}">Back</a>
                    </div>
            </div>
        </div>
            <div style='margin: 10px; text-align: center;'>
                <table class="student-table">
                    <tr>
                        <th>Course Name</th>
                        <th>University</th>
                        <th>Students count</th>
                    </tr>

                    @if(  count($courses) > 0 )
                    @foreach($courses as $course)
                    <tr>
                        <td style=' text-align: left;'>{{ $course[0]->course_name }}</td>
                        <td style=' text-align: left;'>{{$course[0]->university}}</td>
                        <td style=' text-align: left;'>{{$course[0]->total}}</td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="9" style="text-align: center">Oh dear, no data found.</td>
                    </tr>
                    @endif
                </table>
            </div>

        </form>


    </body>

</html>

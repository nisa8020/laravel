<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Students;
use App\Models\Course;

class ExportController extends Controller
{
    public function __construct()
    {

    }

    public function welcome()
    {
        return view('hello');
    }

    /**
     * View all students found in the database
     */
    public function viewStudents()
    {
        $students = Students::with('course','address')->get();

        return view('view_students', compact(['students']));
    }


    /**
     * Exports all student data to a CSV file
     */
    public function exportStudentsToCSV(Request $request)
    {
        $url='';
        $students=[];

        if (isset($request->all()['studentId'])) {

            foreach ( $request->all()['studentId'] as $student_id){
                $students[] = DB::table('student')->where('id', $student_id)->first();;
            }
            $url='csv/'.date('H_i_s_').'students.csv';
            $fp = fopen($url, 'w');

            foreach ($students as  $student) {
                fputcsv($fp, (array)$student);
            }

            fclose($fp);
        }

        return view('view_export', compact(['url'],['students']));

    }

    public function viewCourse()
    {
        $course = Course::with('students')->get();

        return view('view_course', compact(['course']));
    }

    /**
     * Exports the total amount of students that are taking each course to a CSV file
     */
    public function exporttCourseAttendenceToCSV(Request $request)
    {
        $url='';
        $courses=[];
        //courseId

        if (isset($request->all()['courseId'])) {

            foreach ( $request->all()['courseId'] as $course_id){
                $courses[] =DB::table('course')->leftJoin('student','course.id', '=', 'student.course_id' )->select('course.id', 'course.course_name',  'course.university', DB::raw('count(student.course_id) as total') ) ->groupBy('course.id')->where('course.id', $course_id)->get();
            }
            $url='csv/'.date('H_i_s_').'course.csv';

            $fp = fopen($url, 'w');

            foreach ($courses as  $course) {
                fputcsv($fp, (array)$course[0]);
            }

            fclose($fp);

        }

        return view('view_export_course', compact(['url'],['courses']));


    }
}

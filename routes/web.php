<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('', [ 'uses' => 'ExportController@welcome', 'as' => 'home'] );
Route::get('view', [ 'uses' => 'ExportController@viewStudents', 'as' => 'view'] );
Route::get('course', [ 'uses' => 'ExportController@viewCourse', 'as' => 'course'] );
Route::post('export/course', [ 'uses' => 'ExportController@exporttCourseAttendenceToCSV', 'as' => 'export/course'] );
Route::post('export', [ 'uses' => 'ExportController@exportStudentsToCSV', 'as' => 'export'] );